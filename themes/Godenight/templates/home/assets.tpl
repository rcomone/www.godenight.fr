<div id="assets" class="clearfix hidden-sm-down">
    <div class="container">
        <div class="row">
            <div class="services">
                <div class="service-img">
                    <img src="{$urls.img_ps_url}icone-atout-discret-x2.png" alt="">
                </div>
                <div class="service-block">
                    <div class="service-title">{l s='Colis Discret Neutre' d='Shop.Theme.Godenight'}</div>
                    <div class="service-desc">{l s='sans marquage extérieur' d='Shop.Theme.Godenight'}</div>
                </div>
            </div>
            <div class="services">
                <div class="service-img">
                    <img src="{$urls.img_ps_url}icone-atout-secure-x2.png" alt="">
                </div>
                <div class="service-block">
                    <div class="service-title">{l s='Paiement 100% Sécurisé' d='Shop.Theme.Godenight'}</div>
                    <div class="service-desc">{l s='Carte bancaire, PayPal' d='Shop.Theme.Godenight'}</div>
                </div>
            </div>
            <div class="services">
                <div class="service-img">
                    <img src="{$urls.img_ps_url}icone-atout-livraison-x2.png" alt="">
                </div>
                <div class="service-block">
                    <div class="service-title">{l s='Livraison Gratuite' d='Shop.Theme.Godenight'}</div>
                    <div class="service-desc">{l s='À partir de 85€ d’achat' d='Shop.Theme.Godenight'}</div>
                </div>
            </div>
            <div class="services hidden-md-down">
                <div class="service-img">
                    <img src="{$urls.img_ps_url}icone-atout-messenger-x2.png" alt="">
                </div>
                <div class="service-block">
                    <div class="service-title">{l s='Conseils par Messenger' d='Shop.Theme.Godenight'}</div>
                    <div class="service-desc">{l s='Et par email' d='Shop.Theme.Godenight'}</div>
                </div>
            </div>
        </div>
    </div>
</div>
