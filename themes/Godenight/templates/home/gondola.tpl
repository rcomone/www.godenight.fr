<div id="gondola" class="clearfix">
    <div class="container">
        <div class="col-md-6">
            <div class="gondola-content">
                <a href="{$urls.base_url}99-ensembles">
                    <img src="{$urls.img_ps_url}gondola-head/banner-large_ensembles-lingerie_action.png" alt="Ensembles lingerie">
                </a>
                <img src="{$urls.img_ps_url}gondola-head/banner-large_ensembles-lingerie_fond.jpg" alt="Ensembles lingerie" class="hover-img">
            </div>
        </div>
        <div class="col-md-6">
            <div class="gondola-content">
                <a href="{$urls.base_url}12-sextoys">
                    <img src="{$urls.img_ps_url}gondola-head/banner-medium_sextoys-pour-elle_action.png" alt="Sextoys pour elle">
                </a>
                <img src="{$urls.img_ps_url}gondola-head/banner-medium_sextoys-pour-elle_fond.jpg" alt="Sextoys pour elle" class="hover-img">
            </div>
        </div>
        <div class="col-md-6">
            <div class="gondola-content">
                <a href="{$urls.base_url}138-huiles-et-cremes-de-massage">
                    <img src="{$urls.img_ps_url}gondola-head/banner-medium_huiles-et-cremes-de-massage_action.png" alt="Huiles et crèmes de massage ">
                </a>
                <img src="{$urls.img_ps_url}gondola-head/banner-medium_huiles-et-cremes-de-massage_fond.jpg" alt="Huiles et crèmes de massage " class="hover-img">
            </div>
        </div>
    </div>
</div>
