{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{block name='header_banner'}
  <div class="header-banner">
    {hook h='displayBanner'}
  </div>
{/block}

{block name='header_nav'}
  <nav class="header-nav">
    <div class="container">
        <div class="row">
          <div class="hidden-sm-down">
            <div class="col-xs-12 right-nav">
              {hook h='displayNav1'}
                <ul class="list-link">
                    <li>
                        <a href="{$link->getCMSLink('3')}#livraison">
                            <i class="fa fa-cube"></i> {l s='livraison' d='Shop.Theme.Godenight'}
                        </a>
                    </li>
                    <li>
                        <a href="{$link->getCMSLink('3')}#paiement">
                            <i class="fa fa-lock"></i> {l s='Paiement 100%% sécurisé' d='Shop.Theme.Godenight'}
                        </a>
                    </li>
                    <li class="hidden-md-down">
                        <i class="fa fa-comment-o"></i> {l s='conseil' d='Shop.Theme.Godenight'}
                    </li>
                    <li class="hidden-md-down">
                        <a href="#footer">
                            <i class="fa fa-envelope-o"></i> {l s='newsletter' d='Shop.Theme.Godenight'}
                        </a>
                    </li>
                    <li class="hidden-md-down">
                        <a href="{$link->getPageLink('manufacturer')}">
                            <i class="fa fa-registered"></i> {l s='marques' d='Shop.Theme.Godenight'}
                        </a>
                    </li>
                </ul>
            </div>
          </div>
          <div class="hidden-md-up text-sm-center mobile">
            <div class="float-xs-left" id="menu-icon">
              <i class="material-icons d-inline">&#xE5D2;</i>
            </div>
            <div class="float-xs-right" id="_mobile_cart"></div>
            <div class="float-xs-right" id="_mobile_user_info"></div>
            <div class="top-logo" id="_mobile_logo"></div>
            <div class="clearfix"></div>
          </div>
        </div>
    </div>
  </nav>
{/block}

{block name='header_top'}
  <div class="header-top">
    <div class="container">
       <div class="row">
        <div class="col-xl-3 col-md-4 hidden-sm-down" id="_desktop_logo">
            {if $smarty.server.REQUEST_URI == '/'}
                <h1 class="logo-h1" style="background-image: url('{$shop.logo}')">
                    {$shop.name}
                </h1>
            {else}
                <a href="{$urls.base_url}">
                    <img class="logo" src="{$shop.logo}" alt="{$shop.name}">
                </a>
            {/if}
        </div>
           <div class="col-xl-5 offset-xl-1 col-md-5">
               {hook h='displaySearch'}
           </div>
           <div class="col-md-3 displaynav2 hidden-sm-down">
               {hook h='displayNav2'}
           </div>
           {*
        <div class="col-lg-9 col-md-10 col-sm-12 position-static">
          <div class="row">
              <div class="clearfix"></div>
          </div>
        </div>*}
           <div class="col-xs-12">
               <div class="row">
                   {hook h='displayTop'}
                   <div class="clearfix"></div>
               </div>
           </div>
      </div>
      <div id="mobile_top_menu_wrapper" class="row hidden-md-up" style="display:none;">
        <div class="js-top-menu mobile" id="_mobile_top_menu"></div>
        <div class="js-top-menu-bottom">
          <div id="_mobile_currency_selector"></div>
          <div id="_mobile_language_selector"></div>
          <div id="_mobile_contact_link"></div>
        </div>
      </div>
    </div>
  </div>
  {hook h='displayNavFullWidth'}
{/block}





{* COMPRENDRE LE RESPONSIVE SUR CE TEMPLATE *}
{if $urls.base_url != 'https://www.godenight.fr/'}
<div style="position: fixed; background-color: black; color: white; left: 0; right: 0; bottom: 0; z-index: 1000000; text-align: center">
    <span class="hidden-sm-up"> XS </span>
    <span class="hidden-xs-down hidden-md-up"> SM </span>
    <span class="hidden-sm-down hidden-lg-up"> MD </span>
    <span class="hidden-md-down hidden-xl-up"> LG </span>
    <span class="hidden-lg-down"> XL </span>
</div>
{/if}