{*
*  @author    Miguel Costa for emotionLoop
*  @copyright emotionLoop
*}
{if !empty($content)}
    <div class="alert alert-warning">
        {$content nofilter}
    </div>
{/if}