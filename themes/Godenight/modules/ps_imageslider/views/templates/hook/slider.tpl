{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}




{if $homeslider.slides}
  <div id="carousel" data-ride="carousel" class="carousel slide" data-interval="{$homeslider.speed}" data-wrap="{(string)$homeslider.wrap}" data-pause="{$homeslider.pause}">
    <ul class="carousel-inner" role="listbox">
      {foreach from=$homeslider.slides item=slide name='homeslider'}
        <li class="carousel-item {if $smarty.foreach.homeslider.first}active{/if}" role="option" aria-hidden="{if $smarty.foreach.homeslider.first}false{else}true{/if}">

            {* Variabe défaut *}
            {assign var="baseUrlSlide" value=$urls.base_url}
            {assign var="targetSlide" value=''}

            {* Suppression du HTTP ou HTTPS *}
            {*{if $slide.url|truncate:5:"" == 'https'}*}
                {*{assign var="trunc" value=$slide.url|truncate:8:''}*}
            {*{else}*}
                {*{assign var="trunc" value=$slide.url|truncate:7:''}*}
            {*{/if}*}

            {* Si url en HTTP/HTTPS : vers extérieure *}
            {if $slide.url|truncate:4:"" == 'http'}
                {assign var="baseUrlSlide" value=''}
                {assign var="targetSlide" value=' target=_blank'}
            {/if}

            {* Affichage test *}
            {*<div class="alert alert-danger" style="position: relative; z-index: 1;">*}
                {*{$slide.url}*}
            {*</div>*}

          <a href="{$baseUrlSlide}{$slide.url}" style="background-image: url('{$slide.image_url}'); background-color: {$slide.legend}"{$targetSlide}>
            <figure>
              {*<img src="{$slide.image_url}" alt="{$slide.legend|escape}">*}
              {if $slide.title || $slide.description}
                <figcaption class="caption">
                  <h2 class="display-1 text-uppercase">{$slide.title}</h2>
                  <div class="caption-description">{$slide.description nofilter}</div>
                </figcaption>
              {/if}
            </figure>
          </a>
        </li>
      {/foreach}
    </ul>
    <div class="direction" aria-label="{l s='Carousel buttons' d='Shop.Theme.Global'}">
      <a class="left carousel-control" href="#carousel" role="button" data-slide="prev" title="{l s='Previous' d='Shop.Theme.Global'}">
          <i class="material-icons">&#xE5CB;</i>
      </a>
      <a class="right carousel-control" href="#carousel" role="button" data-slide="next" title="{l s='Next' d='Shop.Theme.Global'}">
          <i class="material-icons">&#xE5CC;</i>
      </a>
    </div>
  </div>
{/if}
